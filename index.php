<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

	#*****************************************************************************
	#
	# index.php
	#
	# Author: 		Martin Oberhuber
	# Date:			2006-02-01
	#
	# Description: TM main page
	#
	# 2013-01-08 dwd - minor changes testing development setup
	# 2013-01-08 dwd - minor text changes, fixing Juno SR2 reference 
	#
	#****************************************************************************
	
	#
	# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "Target Management Home";
	$pageKeywords	= "device, target";
	$pageAuthor		= "Martin Oberhuber";
	
	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
	# $Nav->addNavSeparator("My Page Links", 	"downloads.php");
	# $Nav->addCustomNav("My Link", "mypage.php", "_self", 3);
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank", 3);

	# End: page-specific settings
	#
		
	# Paste your HTML content between the EOHTML markers!	
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>
			$pageTitle 
		</h1>
		<h2>
			Mission Statement 
		</h2>
		<p>
			The Target Management project creates data models and frameworks to configure and manage remote systems (from mainframe to embedded), their connections, and their services. 
		</p>
		<h3>
			The Vision 
		</h3>
		<p>
			To be the Eclipse <i>"Explorer of the Network Neighborhood"</i>, with pluggable information providers under a single, consistent UI. Interactively discover, drill down, analyze remote systems (from mainframes down to embedded systems), and provide the context for more advanced actions being plugged in to it. 
		</p>
		<h3>
			The Toolkit 
		</h3>
		<p>
		    The <a href="http://marketplace.eclipse.org/content/tm-terminal">TM Terminal</a> has received excellent reviews
		    and star ratings on the Eclipse Marketplace. It provides a small, re-usable component for Terminal emulation
		    and remote access.</p><p>
		    The <a href="http://tmober.blogspot.com/2006/11/remote-system-explorer-10-is-released.html">Remote System Explorer
		    (RSE)</a> framework integrates any sort of heterogeneous remote resources through a concept of pluggable subsystems.
		    The base toolkit includes a Remote Files subsystem that allows <a href="http://eclipsewebmaster.blogspot.com/2007/01/remote-editing-using-eclipse.html">transparent working on remote computers</a>
		    just like the local one, a shell and a processes subsystem. RSE is in maintenance mode, with possible successors
		    being the <a href="/tcf">TCF Target Explorer</a> or a new framework based on the
		    <a href="https://projects.eclipse.org/projects/tools.tm/reviews/4.0.0-release-review">org.eclipse.remote API</a>.
		</p>
		<p>
			Vendors are extending the RSE with custom subsystems for debugging, remote VNC display and other uses. 
		</p>
		<h3>
			Releases 
		</h3>
		<p>
			Our latest full release is TM 4.4, but only the Terminal received significant updates.
			RSE is still in maintenance mode and remains at version 3.7.100 (updates for Photon).
			See also the <a href="/tm/downloads.php">downloads page</a>. 
		</p>
		<div class="homeitem">
			<h3>
				Quick Links 
			</h3>
			<ul class="midlist">
				<li><a href="http://wiki.eclipse.org/TM" target="_blank"><b>Wiki</b></a> | We use the Wiki extensively for collaboration. Find ongoing discussions, meeting notes and other "not so official" stuff there.</li>
				<li><a href="news://news.eclipse.org/eclipse.tm" target="_blank"><b>Newsgroup</b></a> | For general questions and community discussion (<a href="http://www.eclipse.org/newsportal/thread.php?group=eclipse.tm">Web access</a>, <a href="http://dev.eclipse.org/newslists/news.eclipse.tm/maillist.html">archive</a>).</li>
				<li><a href="http://dev.eclipse.org/mailman/listinfo/tm-dev" target="_blank"><b>Mailing List</b></a> | For project development discussions.</li>
				<li><a href="/tm/development/bug_process.php" target="_blank"><b>Bugs</b></a> | View <a href="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&amp;product=Target+Management&amp;bug_status=NEW&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED&amp;cmdtype=doit">all open</a> issues | <a target="_top" href="https://bugs.eclipse.org/bugs/enter_bug.cgi?product=Target%20Management&amp;version=unspecified&amp;component=RSE">Submit new</a> bugs | Request an <a target="_top" href="https://bugs.eclipse.org/bugs/enter_bug.cgi?product=Target%20Management&amp;version=unspecified&amp;component=RSE&amp;rep_platform=All&amp;op_sys=All&amp;priority=P3&amp;bug_severity=enhancement&amp;form_name=enter_bug">enhancement</a> </li>
				<li><a href="/tm/doc/TM_Use_Cases_v1.1c.pdf"><b>Use cases</b></a> and requirements for Target Management</a></li>
				<li><a href="http://www.eclipse.org/downloads/download.php?file=/tm/presentations/2006-9-29_SummitEurope_TMOverview.pdf"> <b>Architectural Overview</b></a> (<a href="http://www.eclipse.org/downloads/download.php?file=/tm/presentations/2006-9-29_SummitEurope_TMOverview.ppt">PPT</a> | <a href="http://www.eclipse.org/downloads/download.php?file=/tm/presentations/2006-9-29_SummitEurope_TMOverview.pdf">PDF</a>). </li>
				<li><a href="/tm/development/plan.php"><b>TM Project Plans</b></a></li>
				<li><a href="/tools/eclipsetools-charter.php"><b>Tools Project Charter</b></a></li>
			</ul>
		</div>
		<div class="homeitem">
			<h3>
				Events 
			</h3>
			<ul class="midlist">
				<li>Monthly developer phone conference, every 1st wednesday of the month, 9am PST (See the <a href="http://wiki.eclipse.org/TM">Wiki</a> for actual agenda and details)</li>
				<li> <b>March 17-20, 2008</b>: <a href="http://www.eclipsecon.org/2008">EclipseCon 2008</a> - 
				<ul>
					<li><a href="http://www.eclipsecon.org/2008/?page=sub/&amp;id=38" target="_blank"> <b>Remote access with the Target Management Project</b></a>, Tutorial by Martin Oberhuber (Wind River) (slides: <a href="http://www.eclipse.org/downloads/download.php?file=/tm/presentations/TM_Tutorial_ECon08.ppt">PPT</a> 757 KB | <a href="http://www.eclipse.org/downloads/download.php?file=/tm/presentations/TM_Tutorial_ECon08.pdf">PDF</a> 639 KB) | (code: <a href="http://www.eclipse.org/downloads/download.php?file=/tm/presentations/tcf-0.2.0.zip">tcf-0.2.0.zip</a> 3.7 MB | <a href="http://www.eclipse.org/downloads/download.php?file=/tm/presentations/TM_Tutorial_Econ08_samples.zip">tmtutorial.zip</a> 465 KB)</li>
					<li><a href="http://www.eclipsecon.org/2008/?page=sub/&amp;id=39" target="_blank"> <b>Target Management New and Noteworthy</b></a>, Short Talk by Martin Oberhuber (Wind River), TM project lead (slides: <a href="http://www.eclipse.org/downloads/download.php?file=/tm/presentations/TM_Short_ECon08.ppt">PPT</a> 707 KB | <a href="http://www.eclipse.org/downloads/download.php?file=/tm/presentations/TM_Short_ECon08.pdf">PDF</a> 581 KB) 
				</ul>
				</li>
				<li> <b>October 9-11, 2007</b>: Eclipse Summit Europe 2007 - 
				<ul>
					<li><a href="http://www.eclipsecon.org/summiteurope2007/index.php?page=detail/&amp;id=21" target="_blank"> <b>The Target Management Project</b></a>, long talk by Martin Oberhuber (slides: <a href="http://www.eclipse.org/downloads/download.php?file=/tm/presentations/2007-10-10_TM_ESE2007.ppt">PPT</a> | <a href="http://www.eclipse.org/downloads/download.php?file=/tm/presentations/2007-10-10_TM_ESE2007.pdf">PDF</a>) </li>
				</ul>
				</li>
				<li><b>April 12, 2007</b>: <a href="http://live.eclipse.org/node/229">Webinar</a>: TM goals, architecture, future plans and online demo (<a href="http://live.eclipse.org/node/229">50 minute full recording</a> | <a href="http://www.eclipse.org/projects/slides/TM_Webinar_Slides_070412.ppt">PPT slides</a>) </li>
				<li><b>Sept. 27, 2006</b>: TM passed its 1.0 Release Review. The Slides are an interesting read for everyone (Slides as <a href="http://www.eclipse.org/downloads/download.php?file=/tm/presentations/TM_1.0_Release_Review_v3.ppt">PPT</a> | <a href="http://www.eclipse.org/downloads/download.php?file=/tm/presentations/TM_1.0_Release_Review_v3a.pdf">PDF</a>).</li>
			</ul>
		</div>
	</div>
	<div id="rightcolumn">

<div class="sideitem">
<h3>News</h3>
<p>Sept. 2018: <a href="/tm/downloads.php">TM &amp; RSE 4.5</a> are released with Eclipse Simrel 2018-09.</p>
</div>

<div class="sideitem">
<h3>Terminal Restructuring</h3>
<p>April 25, 2014: TM Terminal has been restructured and is now on the <a href="http://marketplace.eclipse.org/content/tm-terminal">Marketplace</a>.
</div>

		<div class="sideitem">
			<h6>
				Getting Started 
			</h6>
			<ul>
				<li><a href="/tm/tutorial/index.php" target="_self">TM Getting Started</a></li>
				<li><a href="http://www.eclipse.org/downloads/download.php?file=/tm/presentations/2006-9-29_SummitEurope_TMOverview.pdf" target="_self">TM Overview Slides</a></li>
				<li><a href="http://live.eclipse.org/node/229">TM Webinar</a></li>
				<li><a href="/tm/doc/TM_Use_Cases_v1.1c.pdf" target="_self">TM Use Cases</a></li>
				<li><a href="/tm/development/plan.php">TM Project Plan</a></li>
				<li><a href="/tm/TM_press_text_2006_06.php">Press text - June 2006</a> </li>
			</ul>
		</div>
	</div>
</div>

EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
