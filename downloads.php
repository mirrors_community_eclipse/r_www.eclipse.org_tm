<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

	#*****************************************************************************
	#
	# downloads.php
	#
	# Author: 		Martin Oberhuber, Nick Boldt
	# Date:			2006-02-01, 2018-06-27
	#
	# Description: TM downloads page
	#
	#
	#****************************************************************************
	
	#
	# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "TM Downloads";
	$pageKeywords	= "device, target";
	$pageAuthor		= "Martin Oberhuber";
	
	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
	# $Nav->addNavSeparator("My Page Links", 	"downloads.php");
	# $Nav->addCustomNav("My Link", "mypage.php", "_self", 3);
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank", 3);

	# End: page-specific settings
	#
		
	# Paste your HTML content between the EOHTML markers!	
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>

<p>All downloads are provided under the terms and conditions of the
 <a href="/legal/epl/notice.php">Eclipse Foundation Software User Agreement</a>
  unless otherwise specified.</p>

<h2>Current releases and CI builds</h2>

<h4>Latest CI Builds</h4>
<p>Compatible with Eclipse 4.12 (Simrel 2019-06)</p>

<ul>
<li>Terminal & RSE 4.5.101: <a href="https://ci.eclipse.org/tm/view/terminal/job/tm-terminal-rse_master/">CI build</a>
</li>
</ul>

<br/>

<h4>TM Terminal &amp; RSE 4.5.100</h4>
<p>Compatible with Eclipse 4.11 (Simrel 2019-03)</p>
<ul>
<li><a href="http://download.eclipse.org/tm/updates/4.5.100-SNAPSHOT/repository/">update site</a> or
		<a href="http://download.eclipse.org/tm/updates/4.5.100-SNAPSHOT/tm-repository-4.5.100-SNAPSHOT.zip">repo zip</a>
</ul>

<hr/>

<h2>Old Releases</h2>

<h4>TM Terminal &amp; RSE 4.5.1</h4>
<p>Compatible with Eclipse 4.10 (Simrel 2018-12)</p>
<ul>
<li><a href="http://download.eclipse.org/tm/updates/4.5.1-SNAPSHOT/repository/">update site</a> or
		<a href="http://download.eclipse.org/tm/updates/4.5.1-SNAPSHOT/tm-repository-4.5.1-SNAPSHOT.zip">repo zip</a>
</ul>

<h4>TM Terminal &amp; RSE 4.5.0</h4>
<p>Compatible with Eclipse 4.9 (Simrel 2018-09)</p>
<ul>
<li><a href="http://download.eclipse.org/tm/updates/4.5.0/repository/">update site</a> or
		<a href="http://download.eclipse.org/tm/updates/4.5.0/tm-repository-4.5.0.zip">repo zip</a>
</ul>

<h4>TM Terminal 4.4 and RSE 3.7.100</h4>
<p>Compatible with Eclipse 4.8 (Photon)</p>
<ul>
<li>Terminal 4.4:
		<a href="http://download.eclipse.org/tm/terminal/updates/4.4milestones/20180611/">update site</a> or 
		<a href="http://download.eclipse.org/tm/terminal/updates/4.4milestones/20180611/org.eclipse.tm.terminal.repo.zip">repo zip</a>
</li>
<li>RSE 3.7.100:
		<a href="http://download.eclipse.org/tm/updates/3.7.100/repository/">update site</a> or 
		<a href="http://download.eclipse.org/tm/updates/3.7.100/rse-repository-3.7.100.zip">repo zip</a>
</ul>

<br/>

<h4>TM Terminal 4.3 and RSE 3.7.3 </h4>
<p>Compatible with Eclipse 4.7.3 (Oxygen.3a)</p>
<ul>
<li>Terminal 4.3:
		<a href="http://download.eclipse.org/tm/terminal/updates/4.3/GA/">update site</a> or 
		<a href="http://download.eclipse.org/tm/terminal/updates/4.3/GA/org.eclipse.tm.terminal.repo.zip">repo zip</a>
</li>
<li>RSE 3.7.3:
		<a href="http://download.eclipse.org/tm/updates/4.3milestones/20170425/">update site</a> or 
		<a href="http://download.eclipse.org/tm/updates/4.3milestones/20170425/org.eclipse.tm.repo.zip">repo zip</a>
</ul>

<br/>

<h4>TM 4.0 (Mars)</h4>
<p>Compatible with Eclipse 3.8.2 and later. See the <a href="https://projects.eclipse.org/projects/tools.tm/reviews/4.0.0-release-review">Release Review</a>.</p>
<ul>
<li>Terminal <a href="http://marketplace.eclipse.org/content/tm-terminal">Marketplace</a> |
  <a href="http://download.eclipse.org/tm/terminal/updates/4.0">p2 update site</a> |
  <a href="/downloads/download.php?file=/tm/terminal/updates/4.0/org.eclipse.tm.terminal-4.0.0.zip">org.eclipse.tm.terminal-4.0.0.zip</a> (1.4 MiB)</li>
<li>RSE p2 software repository: <a href="http://download.eclipse.org/tm/updates/4.0">http://download.eclipse.org/tm/updates/4.0</a>
</ul>

<h3>Archived Releases</h3>
Older releases are found on 
<ul>
	<li><a href="http://download.eclipse.org/tm/downloads/">The TM Download Site</a>
	<li><a href="http://archive.eclipse.org/tm/downloads/">The TM Archive Site</a>
	<li>The TM Update Sites:<ul>
	  <li>http://archive.eclipse.org/tm/updates/R3.x/3.7 or <a href="http://archive.eclipse.org/tm/updates/R3.x/3.7/GA/org.eclipse.tm.repo.zip">repo zip</a> (Luna SR2)
	  <li>http://archive.eclipse.org/tm/updates/R3.x/3.6  (Luna SR1)
	  <li>http://archive.eclipse.org/tm/updates/R3.x/3.5  (Kepler)
	  <li>http://archive.eclipse.org/tm/updates/R3.x/3.4  (Juno)
	  <li>http://archive.eclipse.org/tm/updates/R3.x/3.3  (Indigo)
	  <li>http://archive.eclipse.org/tm/updates/R3.x/3.2  (Helios)
	  <li>http://archive.eclipse.org/tm/updates/R3.x/3.1  (Galileo)
      <li>http://archive.eclipse.org/tm/updates/R3.x/3.0  (Ganymede)
	</ul></li>
	<li>Legacy: <a href="http://download.eclipse.org/tm/updates/2.0">TM 2.x update site</a>)
</ul>

		<p>Older downloads have been submitted to eclipse.org via bugzilla:
		<p/>
		<ul>
		<li>Bugzilla <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=65471">65471</a>:
			Remote System Framework (RSF) 2.0.0 code and 
			<a href="https://bugs.eclipse.org/bugs/attachment.cgi?id=18820">presentation</a>
			</li>
		</ul>
		<p/>
		In addition to that, <a href="/tm/doc/index.php">Developer Documents</a> are available for download.
		<p/>
	</div>
	<div id="rightcolumn">
<div class="sideitem">
<h4>News</h4>
<p>Sept. 2018: <a href="/tm/downloads.php">TM &amp; RSE 4.5</a> are released with Eclipse Simrel 2018-09.</p>
</div>

<div class="sideitem">
<h3>Terminal Restructuring</h3>
<p>April 25, 2014: TM Terminal has been restructured and is now on the <a href="http://marketplace.eclipse.org/content/tm-terminal">Marketplace</a>.
</div>
		<div class="sideitem">
			<h6>Getting started</h6>
			<ul>				
				<li><a href="/tm/meetingnotes/ff01_chicago/DSDPTM_Overview.ppt"
					target="_self">TM Overview Presentation</a></li>
				<li><a href="http://www.eclipse.org/project-slides/TM_3.0_Release_Review.pdf" target="_self">
				    TM 3.0 Release Review Slides</a></li>
				<li><a href="http://www.eclipse.org/downloads/download.php?file=/tm/presentations/TM_2.0_Release_Review.ppt" target="_self">
				    TM 2.0 Release Review Slides</a></li>
				<li><a
					href="/tm/doc/TM_Use_Cases_v1.1c.pdf"
					target="_self">TM Use Cases Document</a></li>
				<!-- <li><a href="/tm/development/index.php">Developer Resources</a></li> -->
				<li><a href="/tm/doc/index.php">Developer Documents</a></li>
			</ul>
		</div>
	</div>
</div>


EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
